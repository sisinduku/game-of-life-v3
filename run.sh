#!/bin/sh
file_inputs=$1

gradle clean build
gradle test jacocoTestReport

if [ $# == 0 ]; then
    java -jar build/libs/ConwayGameOfLifeV3.jar
else
    java -jar build/libs/ConwayGameOfLifeV3.jar $file_inputs
fi