package com.gojek.bootcamp.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

public class GridTest {
    @Test
    public void gridShouldBeAbleToGenerateNextGenerationSeed() {
        Grid grid = new Grid(new int[][] {{-2, 1}, {-1, 1}, {0, 1}, {-2, 2}, {0, 2}, {-2, 3}, {-1, 3}, {0, 3}});
        assertThat(grid.generateLivingCell(new Cell(2, 2)), is(true));
    }

    @Test
    public void gridShouldBeAbleToCheckLivingCell() {
        Grid grid = new Grid(new int[][] {{-2, 1}, {-1, 1}, {0, 1}, {-2, 2}, {0, 2}, {-2, 3}, {-1, 3}, {0, 3}});
        assertThat(grid.isCellAlive(new Cell(0, 2)), is(true));
    }

    @Test
    public void gridShouldBeAbleToCheckDeadCell() {
        Grid grid = new Grid(new int[][] {{-2, 1}, {-1, 1}, {0, 1}, {-2, 2}, {0, 2}, {-2, 3}, {-1, 3}, {0, 3}});
        assertThat(grid.isCellAlive(new Cell(0, 20)), is(false));
    }

    @Test
    public void gridShouldBeAbleToStartNextGeneration() {
        Grid grid = new Grid(new int[][] {{-2, 1}, {-1, 1}, {0, 1}, {-2, 2}, {0, 2}, {-2, 3}, {-1, 3}, {0, 3}});
        assertThat(grid.startNextGeneration(), is(true));
    }

    @Test
    public void gridShouldBeAbleToCountSumOfLivingNeighbours() {
        Grid grid = new Grid(new int[][] {{1, 1}, {1, 2}});
        HashMap<Cell, Integer> expectedValue = new HashMap<>();
        expectedValue.put(new Cell(0, 0), 1);
        expectedValue.put(new Cell(1, 0), 1);
        expectedValue.put(new Cell(2, 0), 1);
        expectedValue.put(new Cell(0, 1), 2);
        expectedValue.put(new Cell(2, 1), 2);
        expectedValue.put(new Cell(0, 2), 2);
        expectedValue.put(new Cell(1, 2), 1);
        expectedValue.put(new Cell(2, 2), 2);
        expectedValue.put(new Cell(1, 1), 1);
        expectedValue.put(new Cell(0, 3), 1);
        expectedValue.put(new Cell(1, 3), 1);
        expectedValue.put(new Cell(2, 3), 1);
        assertThat(grid.countSumOfLivingNeighbours().keySet(), is(equalTo(expectedValue.keySet())));
    }

    @Test
    public void gridWithItselfShouldBeEqual() {
        Grid grid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(grid.equals(grid), is(true));
    }

    @Test
    public void gridWithNullShouldNotBeEqual() {
        Grid grid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(grid.equals(null), is(false));
    }

    @Test
    public void gridWithOtherTypeShouldNotBeEqual() {
        Grid grid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(grid.equals(1), is(false));
    }

    @Test
    public void gridWithSameLivingCellsShouldBeEqual() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        Grid secondGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(firstGrid.equals(secondGrid), is(true));
    }

    @Test
    public void gridWithSameLivingCellsShouldHaveSameHash() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        Grid secondGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(firstGrid.hashCode(), is(equalTo(secondGrid.hashCode())));
    }

    @Test
    public void gridWithSameLivingCellsShouldHaveDifferentHash() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        Grid secondGrid = new Grid(new int[][] {{1, 1}, {2, 1}});
        assertThat(firstGrid.hashCode(), is(not(equalTo(secondGrid.hashCode()))));
    }

    @Test
    public void griShouldBeAbleToReturnItsLivingCells() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        Grid secondGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(firstGrid.getLivingCells(), is(equalTo(secondGrid.getLivingCells())));
    }

    @Test
    public void minx() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {-5, 2}});
        assertThat(firstGrid.getMinimumXValue(), is(equalTo(-5)));
    }

    @Test
    public void miny() {
        Grid firstGrid = new Grid(new int[][] {{1, -5}, {-2, 2}, {-1, -6}, {4, 6}});
        assertThat(firstGrid.getMinimumYValue(), is(equalTo(-6)));
    }

    @Test
    public void maxx() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {7, 2}});
        assertThat(firstGrid.getMaximumX(), is(equalTo(7)));
    }

    @Test
    public void maxy() {
        Grid firstGrid = new Grid(new int[][] {{1, 1}, {1, 2}});
        assertThat(firstGrid.getMaximumY(), is(equalTo(2)));
    }
}