package com.gojek.bootcamp.domain;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

public class CellTest {
    @Test
    public void cellShouldBeAbleToTellAllOfHisNeighboursCoordinates() {
        Cell cell = new Cell(-1, 2);
        int[][] expectedNeighbours = new int[][] {{-2, 1}, {-1, 1}, {0, 1}, {-2, 2}, {0, 2}, {-2, 3}, {-1, 3}, {0, 3}};
        assertThat(cell.getSurroundedNeighbours(), is(equalTo(expectedNeighbours)));
    }

    @Test
    public void cellWithSameXAndYIsEqual() {
        Cell firstCell = new Cell(-1, 2);
        Cell secondCell = new Cell(-1, 2);
        assertThat(firstCell.equals(secondCell), is(true));
    }

    @Test
    public void cellWithItSelfIsEqual() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.equals(firstCell), is(true));
    }

    @Test
    public void cellWithNullIsNotEqual() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.equals(null), is(false));
    }

    @Test
    public void cellWithOtherTypeIsNotEqual() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.equals(1), is(false));
    }

    @Test
    public void cellWithDifferentCellIsNotEqual() {
        Cell firstCell = new Cell(-1, 2);
        Cell secondCell = new Cell(1, 2);
        assertThat(firstCell.equals(secondCell), is(false));
    }

    @Test
    public void cellWithOtherDifferentCellIsNotEqual() {
        Cell firstCell = new Cell(-1, 2);
        Cell secondCell = new Cell(-1, -2);
        assertThat(firstCell.equals(secondCell), is(false));
    }

    @Test
    public void cellWithSameCellShouldHaveSameHash() {
        Cell firstCell = new Cell(-1, 2);
        Cell secondCell = new Cell(-1, 2);
        assertThat(firstCell.hashCode(), is(equalTo(secondCell.hashCode())));
    }

    @Test
    public void cellWithDifferentCellShouldNotHaveSameHash() {
        Cell firstCell = new Cell(-1, 2);
        Cell secondCell = new Cell(1, 2);
        assertThat(firstCell.hashCode(), is(not(equalTo(secondCell.hashCode()))));
    }

    @Test
    public void cellShouldBeAbleToTellItsCoordinate() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(Arrays.equals(firstCell.getCoordinate(), new int[] {-1, 2}), is(true));
    }

    @Test
    public void cellShouldBeAbleToCompareXMoreValue() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.xCompareTo(1), is(equalTo(-1)));
    }

    @Test
    public void cellShouldBeAbleToCompareXLessValue() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.xCompareTo(-3), is(equalTo(1)));
    }

    @Test
    public void cellShouldBeAbleToCompareXSameValue() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.xCompareTo(-1), is(equalTo(0)));
    }

    @Test
    public void cellShouldBeAbleToCompareYMoreValue() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.yCompareTo(3), is(equalTo(-1)));
    }

    @Test
    public void cellShouldBeAbleToCompareYLessValue() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.yCompareTo(-3), is(equalTo(1)));
    }

    @Test
    public void cellShouldBeAbleToCompareYSameValue() {
        Cell firstCell = new Cell(-1, 2);
        assertThat(firstCell.yCompareTo(2), is(equalTo(0)));
    }
}