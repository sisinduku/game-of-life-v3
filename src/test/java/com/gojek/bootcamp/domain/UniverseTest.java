package com.gojek.bootcamp.domain;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

public class UniverseTest {
    @Test
    public void universeShouldBeAbleToReturnNextGenerationGrid() {
        Universe universe = new Universe(new int[][]{{1, 1}, {1, 2}, {1, 3}});
        Grid expectedGrid = new Grid(new int[][] {{2, 2}, {1, 2}, {0, 2}});
        assertThat(universe.runNextGeneration(), is(equalTo(expectedGrid)));
    }

    @Test
    public void universeShouldBeAbleToReturnItsGrid() {
        Universe universe = new Universe(new int[][]{{1, 1}, {1, 2}, {1, 3}});
        Grid expectedGrid = new Grid(new int[][] {{1, 1}, {1, 2}, {1, 3}});
        assertThat(universe.getGrid(), is(equalTo(expectedGrid)));
    }
}