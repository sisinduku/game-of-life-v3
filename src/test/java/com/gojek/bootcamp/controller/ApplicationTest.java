package com.gojek.bootcamp.controller;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

public class ApplicationTest {
    @Test
    public void applicationShouldBeAbleToGenerateSandboxSeed() {
        int[][] seedsMap = {
                {1, 1},
                {2, 2},
                {3, 3},
                {2, 3},
                {4, 4},
                {5, 5}
        };
        assertThat(Arrays.deepEquals(Application.SANDBOX(), seedsMap), is(true));
    }

    @Test
    public void applicationShouldBeAbleToGenerateStillLife() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {1, 3},
                {2, 3}
        };
        assertThat(Arrays.deepEquals(Application.STILL_LIFE(), seedsMap), is(true));
    }

    @Test
    public void BLINKER() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {3, 2}
        };
        assertThat(Arrays.deepEquals(Application.BLINKER(), seedsMap), is(true));
    }

    @Test
    public void BLINKER2() {
        int[][] seedsMap = {
                {3, 1},
                {3, 2},
                {3, 3}
        };
        assertThat(Arrays.deepEquals(Application.BLINKER2(), seedsMap), is(true));
    }

    @Test
    public void GLIDER() {
        int[][] seedsMap = {
                {1, 3},
                {2, 4},
                {0, 5},
                {1, 5},
                {2, 5}
        };
        assertThat(Arrays.deepEquals(Application.GLIDER(), seedsMap), is(true));
    }

    @Test
    public void GLIDERREVERSE() {
        int[][] seedsMap = {
                {1, 3},
                {0, 4},
                {0, 5},
                {1, 5},
                {2, 5}
        };
        assertThat(Arrays.deepEquals(Application.GLIDERREVERSE(), seedsMap), is(true));
    }

    @Test
    public void GLIDERGUN() {
        int[][] seedsMap = {
                {1, 5},
                {2, 5},
                {1, 6},
                {2, 6},
                {11, 5},
                {11, 6},
                {11, 7},
                {12, 4},
                {13, 3},
                {14, 3},
                {12, 8},
                {13, 9},
                {14, 9},
                {15, 6},
                {16, 4},
                {17, 5},
                {17, 6},
                {17, 7},
                {18, 6},
                {16, 8},
                {21, 3},
                {21, 4},
                {21, 5},
                {22, 3},
                {22, 4},
                {22, 5},
                {23, 2},
                {23, 6},
                {25, 1},
                {25, 2},
                {25, 6},
                {25, 7},
                {35, 3},
                {35, 4},
                {36, 3},
                {36, 4}
        };
        assertThat(Arrays.deepEquals(Application.GLIDERGUN(), seedsMap), is(true));
    }
}