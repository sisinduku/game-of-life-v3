package com.gojek.bootcamp.controller;

import com.gojek.bootcamp.domain.Universe;
import com.gojek.bootcamp.presentation.Observatory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/*
 * Class to run game of life
 */
public class Application {
    public static void run(String[] args) throws InterruptedException {
        Universe universe = new Universe(SANDBOX());
        Scanner keyboard = new Scanner(System.in);
        if (args.length > 0) {
            try {
                keyboard = new Scanner(new File(args[0]));
                List<int[]> template = new LinkedList<int[]>();
                int y = 0;
                while (keyboard.hasNextLine()) {
                    String stringLine = keyboard.nextLine();
                    for (int x = 0; x < stringLine.length(); x++) {
                        if (stringLine.charAt(x) == 'O') {
                            template.add(new int[] {x, y});
                        }
                    }
                    y++;
                }
                universe = new Universe(template.toArray(new int[][]{}));
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            }
        } else {
            System.out.println("PICK Your Template");
            System.out.println("1. SANDBOX");
            System.out.println("2. STILL_LIFE");
            System.out.println("3. BLINKER");
            System.out.println("4. BLINKER2");
            System.out.println("5. GLIDER");
            System.out.println("6. GLIDERREVERSE");
            System.out.println("7. GLIDERGUN");
            System.out.println("Pick 1-7");
            int menu = keyboard.nextInt();
            switch (menu){
                case 1:
                    universe = new Universe(SANDBOX());
                    break;
                case 2:
                    universe = new Universe(STILL_LIFE());
                    break;
                case 3:
                    universe = new Universe(BLINKER());
                    break;
                case 4:
                    universe = new Universe(BLINKER2());
                    break;
                case 5:
                    universe = new Universe(GLIDER());
                    break;
                case 6:
                    universe = new Universe(GLIDERREVERSE());
                    break;
                case 7:
                    universe = new Universe(GLIDERGUN());
                    break;
            }
        }
        keyboard = new Scanner(System.in);
        System.out.println("How many generation?");
        int itteration = keyboard.nextInt();

        Observatory observatory = new Observatory(universe);
        for (int i = 0; i <= itteration; i++) {
            System.out.println("Generation - " + i);
            observatory.printUniverseImage();
            System.out.println(" ");
            Thread.sleep(500);
        }
    }

    public static int[][] SANDBOX() {
        int[][] seedsMap = {
                {1, 1},
                {2, 2},
                {3, 3},
                {2, 3},
                {4, 4},
                {5, 5}
        };
        return seedsMap;
    }

    public static int[][] STILL_LIFE() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {1, 3},
                {2, 3}
        };
        return seedsMap;
    }

    public static int[][] BLINKER() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {3, 2}
        };
        return seedsMap;
    }

    public static int[][] BLINKER2() {
        int[][] seedsMap = {
                {3, 1},
                {3, 2},
                {3, 3}
        };
        return seedsMap;
    }

    public static int[][] GLIDER() {
        int[][] seedsMap = {
                {1, 3},
                {2, 4},
                {0, 5},
                {1, 5},
                {2, 5}
        };
        return seedsMap;
    }

    public static int[][] GLIDERREVERSE() {
        int[][] seedsMap = {
                {1, 3},
                {0, 4},
                {0, 5},
                {1, 5},
                {2, 5}
        };
        return seedsMap;
    }

    public static int[][] GLIDERGUN() {
        int[][] seedsMap = {
                {1, 5},
                {2, 5},
                {1, 6},
                {2, 6},
                {11, 5},
                {11, 6},
                {11, 7},
                {12, 4},
                {13, 3},
                {14, 3},
                {12, 8},
                {13, 9},
                {14, 9},
                {15, 6},
                {16, 4},
                {17, 5},
                {17, 6},
                {17, 7},
                {18, 6},
                {16, 8},
                {21, 3},
                {21, 4},
                {21, 5},
                {22, 3},
                {22, 4},
                {22, 5},
                {23, 2},
                {23, 6},
                {25, 1},
                {25, 2},
                {25, 6},
                {25, 7},
                {35, 3},
                {35, 4},
                {36, 3},
                {36, 4}
        };
        return seedsMap;
    }
}
