package com.gojek.bootcamp.presentation;

import com.gojek.bootcamp.domain.Cell;
import com.gojek.bootcamp.domain.Grid;
import com.gojek.bootcamp.domain.Universe;

/*
 * Class to display the universe
 */
public class Observatory {
    private Universe universe;
    private int minXValue = Integer.MAX_VALUE;
    private int minYValue = Integer.MAX_VALUE;
    private int maxXValue = Integer.MIN_VALUE;
    private int maxYValue = Integer.MIN_VALUE;

    public Observatory(Universe universe) {
        this.universe = universe;
    }

    public void printUniverseImage() {
        setMinAndMaxBoundary(this.universe.getGrid());
        for (int y = minYValue; y <= maxYValue; y++) {
            for (int x = minXValue; x <= maxXValue; x++) {
                if (this.universe.getGrid().isCellAlive(new Cell(x, y)))
                    System.out.print("#");
                else
                    System.out.print("-");
            }
            System.out.println(" ");
        }
        this.universe.runNextGeneration();
    }

    private void setMinAndMaxBoundary(Grid universeGrid) {
        int minX = universeGrid.getMinimumXValue();
        int minY = universeGrid.getMinimumYValue();
        int maxX = universeGrid.getMaximumX();
        int maxY = universeGrid.getMaximumY();

        if (minX <= minXValue ) minXValue = minX;
        if (minY <= minYValue ) minYValue = minY;
        if (maxX >= maxXValue ) maxXValue = maxX;
        if (maxY >= maxYValue ) maxYValue = maxY;
    }
}
