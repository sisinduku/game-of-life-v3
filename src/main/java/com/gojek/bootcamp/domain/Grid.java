package com.gojek.bootcamp.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/*
 * Class to model group of living cells
 */
public class Grid {
    private List<Cell> livingCells;
    private List<Cell> nextGeneretionCells;

    public Grid(int[][] seeds) {
        this.livingCells = new ArrayList<Cell>();
        for (int[] seed:
             seeds) {
            this.livingCells.add(new Cell(seed[0], seed[1]));
        }
        this.nextGeneretionCells = new LinkedList<Cell>();
    }

    public boolean generateLivingCell(Cell cell) {
        return this.nextGeneretionCells.add(cell);
    }

    public boolean startNextGeneration() {
        this.livingCells = nextGeneretionCells;
        this.nextGeneretionCells = new LinkedList<Cell>();
        return true;
    }

    public HashMap<Cell, Integer> countSumOfLivingNeighbours() {
        HashMap<Cell, Integer> sumOfLivingNeighbours = new HashMap<>();

        for (Cell cell: livingCells) {
            int[][] currentLivingCellNeighbours = cell.getSurroundedNeighbours();
            for (int[] neighbour :
                    currentLivingCellNeighbours) {
                Cell currentCellNeighbour = new Cell(neighbour[0], neighbour[1]);
                int currentValue = sumOfLivingNeighbours.getOrDefault(
                        new Cell(neighbour[0], neighbour[1]), 0);
                sumOfLivingNeighbours.put(currentCellNeighbour, currentValue + 1);
            }
        }
        return sumOfLivingNeighbours;
    }

    public boolean isCellAlive(Cell cell) {
        return this.livingCells.contains(cell);
    }

    public int getMinimumXValue() {
        int minimumX = Integer.MAX_VALUE;

        for (Cell cell :
                this.livingCells) {
            if (cell.xCompareTo(minimumX) <= 0) minimumX = cell.getCoordinate()[0];
        }

        return minimumX;
    }

    public int getMinimumYValue() {
        int minimumY = Integer.MAX_VALUE;

        for (Cell cell :
                this.livingCells) {
            if (cell.yCompareTo(minimumY) <= 0) minimumY = cell.getCoordinate()[1];
        }

        return minimumY;
    }

    public int getMaximumX() {
        int maximumX = Integer.MIN_VALUE;

        for (Cell cell :
                this.livingCells) {
            if (cell.xCompareTo(maximumX) > 0) maximumX = cell.getCoordinate()[0];
        }

        return maximumX;
    }

    public int getMaximumY() {
        int maximumY = Integer.MIN_VALUE;

        for (Cell cell :
                this.livingCells) {
            if (cell.yCompareTo(maximumY) > 0) maximumY = cell.getCoordinate()[1];
        }

        return maximumY;
    }

    public List<Cell> getLivingCells() {
        return livingCells;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Grid grid = (Grid) o;

        return livingCells.equals(grid.livingCells);
    }

    @Override
    public int hashCode() {
        return livingCells.hashCode();
    }
}
