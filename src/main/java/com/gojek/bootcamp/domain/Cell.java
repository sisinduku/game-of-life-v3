package com.gojek.bootcamp.domain;

import java.util.LinkedList;
import java.util.List;

/*
 * Class to model each cell in universe
 */
public class Cell {
    private int x, y;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int[][] getSurroundedNeighbours() {
        List<int[]> surroundedNeighbour = new LinkedList<int[]>();
        for (int i = y-1; i <= y+1; i++) {
            for (int j = x-1; j <= x+1; j++) {
                if (i != y || j != x) {
                    surroundedNeighbour.add(new int[] {j, i});
                }
            }
        }
        return surroundedNeighbour.toArray(new int[][]{});
    }

    public int[] getCoordinate() {
        return new int[] {this.x, this.y};
    }

    public int xCompareTo(int x) {
        if (this.x < x) return -1;
        else if (this.x > x) return 1;
        else return 0;
    }

    public int yCompareTo(int y) {
        if (this.y < y) return -1;
        else if (this.y > y) return 1;
        else return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        if (x != cell.x) return false;
        return y == cell.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
