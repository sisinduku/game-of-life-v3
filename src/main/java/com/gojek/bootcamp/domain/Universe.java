package com.gojek.bootcamp.domain;

import java.util.HashMap;
import java.util.Map;

/*
 * Class to model whole universe and itterate each generation
 */
public class Universe {
    private Grid grid;

    public Universe(int[][] initialSeed) {
        this.grid = new Grid(initialSeed);
    }

    public Grid runNextGeneration() {
        HashMap<Cell, Integer> sumOfLivingNeighbours = this.grid.countSumOfLivingNeighbours();
        for (Map.Entry<Cell, Integer> cellPointer : sumOfLivingNeighbours.entrySet()) {
            Cell currentCell = cellPointer.getKey();
            int numberOfLivingNeighbour = cellPointer.getValue();
            if (isLiveForNextGeneration(currentCell, numberOfLivingNeighbour)) {
                this.grid.generateLivingCell(currentCell);
            }
        }
        this.grid.startNextGeneration();
        return this.grid;
    }

    public Grid getGrid() {
        return grid;
    }

    private boolean isLiveForNextGeneration(Cell cell, int numberOfLivingNeighbour) {
        if (this.grid.isCellAlive(cell)) {
            if (numberOfLivingNeighbour == 2 || numberOfLivingNeighbour == 3) {
                return true;
            }
        } else {
            if (numberOfLivingNeighbour == 3) {
                return true;
            }
        }

        return false;
    }
}
