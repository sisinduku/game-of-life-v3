package com.gojek.bootcamp;

import com.gojek.bootcamp.controller.Application;
import com.gojek.bootcamp.domain.Universe;
import com.gojek.bootcamp.presentation.Observatory;

import java.util.Scanner;

/*
 * Class for entry point
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        Application.run(args);
    }
}
