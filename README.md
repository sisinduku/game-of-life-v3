# Game of Life

The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead, or "populated" or "unpopulated". Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
Any live cell with two or three live neighbours lives on to the next generation.
Any live cell with more than three live neighbours dies, as if by overpopulation.
Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

## Table of Content
- [Getting Started](#started)
  - [Quick Start](#quick)
- [Building App](#building)
- [Testing App](#testing) 
- [Running App](#running) 

<a name="started" />
## Getting Started

<a name="quick" />
### Quick Start
</a>
Download and install virtual box:
**https://www.virtualbox.org/wiki/Downloads**

First, install vagrant:
**https://www.vagrantup.com/downloads.html**

#### Initialize Vagrant
```
vagrant init
```

#### Running Vagrant
```
vagrant up
```

#### Install dependencies
```
vagrant provision
```

Enter vagrant :
```
vagrant ssh -- -t /vagrant/
```

<a name="building" />
## Building the app
</a>
To build the app, navigate to the project folder, and build:
```
gradle build
```

<a name="testing" />
## Testing the app
</a>
To test the app, navigate to the project folder and run the following:
```
gradle test
```

<a name="running" />
## Running Game of Life
</a>
You can run app using file seed template
```
java -jar build/libs/ConwayGameOfLifeV3.jar [seed_template]
```

Available template files:
Pi_ship_1.txt
Spacefiller_1.txt

Or you can direcly run the app
```
java -jar build/libs/ConwayGameOfLifeV3.jar
```