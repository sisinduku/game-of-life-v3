sudo yum -y install java-1.8.0-openjdk-devel wget unzip

if [ ! -d "/opt/gradle" ]; then
  wget https://services.gradle.org/distributions/gradle-3.4.1-bin.zip
  sudo mkdir /opt/gradle
  sudo unzip -d /opt/gradle gradle-3.4.1-bin.zip
  sudo ln -s /opt/gradle/gradle-3.4.1/bin/gradle /usr/local/bin/
  export PATH=$PATH:/usr/local/bin
fi

set -e
set -x

sudo fdisk -u /dev/sdb <<EOF
n
p
1


t
8e
w
EOF

pvcreate /dev/sdb1
vgextend VolGroup00 /dev/sdb1
lvextend -l +100%FREE /dev/VolGroup00/LogVol00
xfs_growfs /dev/mapper/VolGroup00-LogVol00

date > /etc/disk_added_date